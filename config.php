<?php
//172.16.23.220
define('ROOT', 'http://localhost/2016-1/spotify.com/');

define('MODELS', '../models/');
define('BS','../bussinesLogic/');

define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'spotify');
define('DB_TYPE', 'mysql');
define('LOCAL_SERVER',false);

if ($source == "services") {

    define('URL', ROOT . 'services/');

    define('HASH_KEY', '1234');
    define('HASH_SECRET', '5678');
    define('HASH_PASSWORD_KEY', '9123');
    define('SECRET_WORD', 'surrender');

    define('LIBS', '../services_libs/');
}

if ($source == "panels") {

    define('URL', ROOT.'AdminPanel/');
    define('MODULE','views/modules/');

    define('HASH_ALGO' , 'sha512');
    define('HASH_KEY' , 'my_key');
    define('HASH_SECRET' , 'my_secret');
    define('SECRET_WORD' , 'so_secret');

}

if ($source == "usbcali") {

    define('URL', ROOT . 'usbcali/');

    define('HASH_KEY', '1234');
    define('HASH_SECRET', '5678');
    define('HASH_PASSWORD_KEY', '9123');
    define('SECRET_WORD', 'surrender');

    define('DB_HOST', 'localhost');
    define('DB_USER', 'root');
    define('DB_PASS', '');
    define('DB_NAME', 'usbcali');
    define('DB_TYPE', 'mysql');

    define('MODELS', './models/');
    define('BS','./bussinesLogic/');

    define('LIBS', '../services_libs/');
}
