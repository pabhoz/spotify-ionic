angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider



      .state('menu.myMusic', {
    url: '/myMusic',
    views: {
      'side-menu21': {
        templateUrl: 'templates/myMusic.html',
        controller: 'myMusicCtrl'
      }
    }
  })

  .state('menu', {
    url: '/side-menu21',
    templateUrl: 'templates/menu.html',
    abstract:true
  })

  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl'
  })

  .state('signup', {
    url: '/signup',
    templateUrl: 'templates/signup.html',
    controller: 'signupCtrl'
  })

  .state('menu.songs', {
    url: '/songs',
    views: {
      'side-menu21': {
        templateUrl: 'templates/songs.html',
        controller: 'songsCtrl'
      }
    }
  })

  .state('menu.search', {
    url: '/search',
    views: {
      'side-menu21': {
        templateUrl: 'templates/search.html',
        controller: 'searchCtrl'
      }
    }
  })

  .state('player', {
    url: '/player',
    templateUrl: 'templates/player.html',
    controller: 'playerCtrl'
  })

$urlRouterProvider.otherwise('/login')



});
