angular.module('app.controllers', [])

.controller('myMusicCtrl', function($scope, $http) {

})

.controller('loginCtrl', function($scope) {

})

.controller('signupCtrl', function($scope) {

})

.controller('songsCtrl', function($scope, $http, $location) {
  $http.get("http://172.16.23.220/2016-1/synesthesiApp/services/Users/mySongs/?key=q6qoqZ2jmpyq&usrId=1")
    .then(function(response) {
        console.log(response);
        $scope.songs = response.data;
    });

    $scope.playSong = function playSong(id){
        $location.path("/player").search({id:id});
    };
})

.controller('searchCtrl', function($scope) {

})

.controller('playerCtrl', function($scope, $http, $location) {

$scope.elapsetTime = 0;

  var params = $location.search();
  console.log(params);
  $http.get("http://172.16.23.220/2016-1/synesthesiApp/services/Songs/?key=q6qoqZ2jmpyq&usrId="+params.id)
    .then(function(response) {
        console.log(response);
        $scope.song = response.data;
        var audio = document.createElement("audio");
        audio.src = response.data.uri;
        $scope.audio = audio;

        $scope.audio.addEventListener("timeupdate",function(){
          console.log($scope.elapsedTime);
      		var totalSec = this.currentTime;
      		var hours = parseInt(totalSec/3600) % 24;
      		var minutes = parseInt(totalSec/60) % 60;
      		var seconds = parseInt(totalSec % 60, 10);

      		var result = (hours < 10 ? "0" + hours : hours) +":"+ (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);

      		$scope.elapsedTime = (( 100 / this.duration ) * this.currentTime);

      	}, false);
    });

    $scope.play = function play($event){
      var button = $event.target;
      if($scope.audio.paused){
        $scope.audio.play();
        button.classList.remove("ion-ios-play");
        button.classList.add("ion-ios-pause");
      }else{
        $scope.audio.pause();
        button.classList.remove("ion-ios-pause");
        button.classList.add("ion-ios-play");
      }

    }
})
