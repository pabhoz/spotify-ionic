<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Song
 *
 * @author PabloAnibal
 */
class Songs extends Model{
    
    protected static $table = "Songs";
    
    private $id;
    private $title;
    private $uri;
    
    function __construct($id, $title, $uri) {
        $this->id = $id;
        $this->title = $title;
        $this->uri = $uri;
    }

    public function getMyVars(){
        return get_object_vars($this);
    }
    
    function getUri() {
        return $this->uri;
    }

    function getId() {
        return $this->id;
    }

    function getTitle() {
        return $this->title;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTitle($title) {
        $this->title = $title;
    }
    
    function setUri($uri){
        $this->uri = $uri;
    }


}
