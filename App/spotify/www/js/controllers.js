angular.module('app.controllers', [])

.controller('myMusicCtrl', function($scope) {

})

.controller('loginCtrl', function($scope) {

})

.controller('signupCtrl', function($scope) {

})

.controller('songsCtrl', function($scope,$http,$location,$state) {
  $http.get("http://172.16.23.220/2016-1/spotify.com/services/Users/mySongs?key=q6qoqZ2jmpyq&usrId=1")
  .then(function(response){
    $scope.songs = response.data;
  });

  $scope.playSong = function playSong(id){
    $location.path("/player").search({id:id});
    //$state.go("player",id,{"enableBack":true});
  };
})

.controller('searchCtrl', function($scope) {

})

.controller('playerCtrl', function($scope,$http,$location) {
    var params = $location.search();
    console.log(params);
    $http.get("http://172.16.23.220/2016-1/spotify.com/services/Songs/?key=q6qoqZ2jmpyq&id="+params.id)
    .then(function(response){
      $scope.song = response.data;
      var player = document.createElement("audio");
      player.src = response.data.uri;
      $scope.player = player;
    });

    $scope.play = function play($event){
      var button = $event.currentTarget;
      if($scope.player.paused){
        $scope.player.play();
        button.classList.remove("ion-ios-play");
        button.classList.add("ion-ios-pause");
      }else{
        $scope.player.pause();
        button.classList.remove("ion-ios-pause");
        button.classList.add("ion-ios-play");
      }
    };
})
