<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Users_controller
 *
 * @author pabhoz
 */
class Songs_controller extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function getSongs($id){

        $song = Songs::getById($id);
        $song->setUri(ROOT.$song->getUri());
        print(json_encode($song->toArray()));
    }

}
