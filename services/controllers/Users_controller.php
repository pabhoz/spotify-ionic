<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Users_controller
 *
 * @author pabhoz
 */
class Users_controller extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function getUsers($id){

        if($id){
            $usr = Usuario::getById($id);
            if($usr == null){exit("{}");}
            print_r(json_encode($usr->toArray()));
        }else{
            print_r(json_encode(Usuario::getAll()));
        }
    }

    public function getMySongs($usrId){

            if(!isset($_GET["usrId"])){
                exit();
            }

            print_r(json_encode(Songs::getAll()));

    }
}
