<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Controller
 *
 * @author Pabhoz
 */
class Controller {
    
    function __construct() {
        $this->view = new View();
        //Session::init();
    }
    
    function validateKeys($keys,$where){
        foreach ( $keys as $key ){
            if(empty($where[$key]) or !isset($where[$key])){
                exit("No se encuentra el campo ".$key."!");
            }
        }
        return true;
    }
    
    public function getTableView(){
        /*switch ($_POST['action']) {
            case 'GET':
                $data = $_POST['class']::getAll();
                $meta = $_POST['class']::getMetaTable();
                Module::constructView($_POST['action'], $meta, $_POST['class'], $data);
                break;
            case 'CREATE':
                $meta = $_POST['class']::getMetaTable();
                Module::constructView($_POST['action'], $meta, $_POST['class']);
                break;
            default:
                break;
        }*/
        Module::constructView($_POST['action'], $_POST['class']);
    }
    
}

spl_autoload_register(function($class){
    if(file_exists("./controllers/".$class.".php")){
        require "./controllers/".$class.".php";
    }
});