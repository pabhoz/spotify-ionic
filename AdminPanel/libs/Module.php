<?php

/**
 * Description of Model
 *
 * @author cahurtado
 *
 */
class Module {

    public function __construct() {
        
    }

    public function constructView($type, $class) {

        switch ($type) {
            case 'GET':
                $data = $class::getAll();
                $meta = $class::getMetaTable();
                Module::getView($data, $meta, $class);
                break;
            case 'CREATE':
                $meta = $class::getMetaTable();
                Module::createView($meta, $class);
                break;
            case 'UPDATE': //PENDIENTE
                $meta = $class::getMetaTable();
                Module::createView($meta, $class);
                break;
            default:
                # code...
                break;
        }
    }

    private function getView($data, $meta, $class) {
        $response = '<div class="row"><div class="col-xs-12"><div class="box">
                    <div class="box-header"><h3 class="box-title">Lista de ' . $class . '</h3>
                    </div><div class="box-body">
                    <table id="dataTable" class="table table-bordered table-hover"><thead><tr>';
        $th = "";
        foreach ($meta as $value) {
            $th .= '<th>' . $value['Field'] . '</th>';
        }
        $response.=$th . '</tr></tr></thead><tbody>';
        foreach ($data as $object) {
            $response.='<tr>';
            foreach ($object as $key => $value) {
                $response.='<td>' . $value . '</td>';
            }
            $response.='</tr>';
        }
        $response.='</tbody><tfoot><tr>' . $th . '</tr></tfoot></table></div></div></div></div>';
        print_r($response);
    }

    private function createView($meta, $class) {

        $response = '<div class="row"><div class="col-xs-12"><div class="box box-default">
                    <div class="box-header"><h3 class="box-title">Crear un ' . $class . '</h3></div>
                    <form id="holis"><div class="box-body">';
        foreach ($meta as $value) {
            $response .= Module::formType($value);
        }

        $response .='</div><div class="box-footer clearfix">
                    <input type="button" onclick="createFunction()" value="Crear" class="btn btn-flat btn-success pull-left">
                    </div></form></div></div></div>';
        print_r($response);
    }

    private function formType($meta) {
        try {
            $explodedVarType = preg_split('/[()]/', strtolower($meta['Type']));
            $varType = $explodedVarType[0];
        } catch (Exception $exc) {
            $varType = $meta['Type'];
        }
        $nullable = ($meta['Null'] === 'NO') ? 'required' : '';
        $unique = ($meta['Key'] === 'UNI') ? ' unique' : '';

        if (strcmp($meta['Key'], 'MUL')) {
            switch ($varType) {
                case 'interger': case 'int': case 'smallint': case 'tinyint': case 'mediumint': case 'bigint':
                    $input = '<div class="form-group"><label for="input' . $meta['Field'] . '">' . $meta['Field'] . '</label>
                          <input type="number" class="form-control' . $unique . '" id="input' . $meta['Field'] . '" placeholder="' . $meta['Field'] . '"' . $nullable . '>
                          </input></div>';
                    break;
                case 'char': case 'varchar':
                    $input = '<div class="form-group"><label for="input' . $meta['Field'] . '">' . $meta['Field'] . '</label>
                          <input type="text" class="form-control' . $unique . '" id="input' . $meta['Field'] . '" placeholder="' . $meta['Field'] . '" maxlength="' . $explodedVarType[1] . '"' . $nullable . '>
                          </input></div>';
                    break;
                case 'blob':

                    break;
                case 'text':

                    break;
                case 'time':

                    break;
                case 'date':

                    break;
                case 'datetime':

                    break;
                default:
                    break;
            }
        } else {
            $explodedName = preg_split('/[_]/', strtolower($meta['Field']));
            $input = '<div class="form-group"><label>' . $explodedName[0] . '</label><select class="form-control"' . $nullable . '>';
            $relationship = $explodedName[0]::getAll();
            foreach ($relationship as $value) {
                $input .= '<option value="'.$value['id'].'">' . $value[$explodedName[0]] . '</option>';
            }
            $input .= '</select></div>';
        }
        return $input;
    }

    public function checkInput(){


    }
}
