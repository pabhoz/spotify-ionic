<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

    <!-- Sidebar user panel (optional) -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php print(URL); ?>public/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>Alexander Pierce</p>
        <!-- Status -->
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>

    <!-- search form (Optional) -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Buscar...">
        <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
        </span>
      </div>
    </form>
    <!-- /.search form -->

    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">
       <li class="header">MENU</li>
      <!-- Optionally, you can add icons to the links -->
      <!--i class="active"><a href="#"><i class="fa fa-link"></i> <span>Link</span></a></li-->
      <li><a href=""><i class="fa fa-home"></i> <span>Inicio</span></a></li>
      <li class="moduleButton" data-class="User" data-action="CREATE"><a href="#"><i class="fa fa-users"></i> <span>Usuarios</span></a></li>
      <li class="treeview">
        <a href=""><i class="fa fa-user"></i> <span>Roles</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
          <li class="moduleButton" data-class="Rol" data-action="GET"><a href="#">Ver Roles</a></li>
          <li class="moduleButton" data-class="Rol" data-action="CREATE"><a href="#">Crear Roles</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href=""><i class="fa fa-calendar"></i> <span>Eventos</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
          <li><a href="">Ver Eventos</a></li>
          <li><a href="">Crear Eventos</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#"><i class="fa fa-university"></i> <span>Actividades</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
          <li><a href="">Ver Actividades</a></li>
          <li><a href="">Crear Actividades</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#"><i class="fa fa-book"></i> <span>Noticias</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
          <li><a href="">Ver Noticias</a></li>
          <li><a href="">Crear Noticias</a></li>
        </ul>
      </li>
    </ul><!-- /.sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</aside>