<?php

class User extends Model{
    
    protected static $table = "User";
    
    private $id;
    private $name;
    private $email;
    private $password;
    
    private $has_one = array(
        
            'Rol' => array(
                'class' => 'Rol',
                'join_as' => 'idRol',
                'join_with' => 'id'
            )
        
    );
            
    function __construct($id, $name, $email, $password) {
        
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
    }
    
    public function getMyVars(){       
        return get_object_vars($this);
    }
    
    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getEmail() {
        return $this->email;
    }

    function getPassword() {
        return $this->password;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setPassword($password) {
        $this->password = $password;
    }


}

